// Weather

const input = document.querySelector('.weather-text');
const button = document.querySelector('.weather-btn');
const card = document.querySelector('.post');
const icon = document.querySelector('.icon');
const error = document.querySelector('.error');
const smile = document.querySelector('.smile');

// делаем запрос на сервер

const ApiKey = '3a3e5b02bc7c16fe3910bbe7a1d726c6';

const url = `https://api.openweathermap.org/data/2.5/weather?units=metric&q=`

async function showWeather(city) {
    const response = await fetch(url + city + `&appid=${ApiKey}`);
    const data = await response.json();
    if (response.status === 404) {
        error.style.display = "flex"
        card.style.display = "none"
    }
    console.log(data)

    // Выводим данные на страницу
    
    document.querySelector('.post-title').textContent = data.name;
    document.querySelector('.temp').textContent = Math.round(data.main.temp);
    document.querySelector('.post-wind').textContent = "wind " +  Math.round(data.wind.speed) + " m/s";
    document.querySelector('.post-disc').textContent = data.weather[0].description;

    if (data.weather[0].main === "Clear") {
        icon.src = 'image/sun.png'
    } else if (data.weather[0].main === "Rain") {
        icon.src = 'image/rain.png'
    } else if (data.weather[0].main === "Clouds") {
        icon.src = 'image/cloud.png'
    } else if (data.weather[0].main === "Snow") {
        icon.src = 'image/snow.png'
    }

    card.style.display = "flex"
    error.style.display = "none"
}

button.addEventListener('click', (e) => {
    e.preventDefault();
    showWeather(input.value);
    input.value = "";
})